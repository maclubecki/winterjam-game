#ifndef GAME_H
#define GAME_H

#include <string>
#include "Engine.h"
#include "Button.h"

#define ui_colors_count 10

enum UI_Colors_t{
	UI_COLOR_INTERFACE,
	UI_COLOR_INPUT,
	UI_COLOR_STAMBAR,
	UI_COLOR_FILTER,
	UI_COLOR_SCORE,
	UI_COLOR_ENEMYMAXHP,
	UI_COLOR_ENEMY2HP,
	UI_COLOR_ENEMY1HP,
	UI_COLOR_ENEMYFRAME,
	UI_COLOR_SNOW
};
struct UI_Textures{
	Texture* stamina_bar;
	Texture* stamina_bar_back;
	Texture* input_text_texture;
	Texture* night_filter;
	Texture* time_text;
	Texture* score_text;
	SDL_Color colors[ui_colors_count];
};

enum Gamestate_t{
	GAMESTATE_MENU,
	GAMESTATE_RUN,
	GAMESTATE_OVER,
	GAMESTATE_PAUSE
};

class Game
{
public:
	Game();
	~Game();
	
	void mainloop();

private:
	int score;
	Engine* main_engine;
	SDL_Point *player_camera;
	std::string input_text;
	Player* temp_hero;
	EntityContainer<Enemy> ec_enemies;
	EntityContainer<Projectile> ec_projs;
	std::vector<Particle*> particles;
	std::vector<std::string> wordbase;
	Uint32 time_last_spawn;
	Uint32 time_game_started;
	UI_Textures ui;
	Gamestate_t gamestate;
	int timer_m;
	int timer_s;
	
	void drawWorld();
	void cleanup();
	void spawnEnemy();
	void executeLogic(const float accumulator);
	void spriteUpdates(int framecount);
	void reset();

	void gameover(bool &quit);
	void run(bool &quit);
	void startMenu(bool &quit);
};

#endif