#ifndef TEXTURE_H
#define TEXTURE_H

#include "SDL.h"
#include "SDL_ttf.h"
#include "SDL_image.h"
#include <string>
#include <vector>

const int CHUDEJ_RESX = 1024;//1920;
const int CHUDEJ_RESY = 768;//1080;

#define NUMBEROF_SPRITESTATES 14

struct Hitbox{
	int x,y,w,h;
	Hitbox():x(0),y(0),w(0),h(0){}
	Hitbox(int x_, int y_, int w_, int h_):x(x_),y(y_),w(w_),h(h_){}
	void set(int x_, int y_, int w_, int h_){x=x_;y=y_;w=w_;h=h_;}
};

enum spriteState_t
{
	SPRITESTATE_IDLE,
	SPRITESTATE_RUN_D,
	SPRITESTATE_RUN_U,
	SPRITESTATE_RUN_L,
	SPRITESTATE_RUN_R,
	SPRITESTATE_FLINCH_D,
	SPRITESTATE_FLINCH_U,
	SPRITESTATE_FLINCH_L,
	SPRITESTATE_FLINCH_R,
	SPRITESTATE_ATTACK_D,
	SPRITESTATE_ATTACK_U,
	SPRITESTATE_ATTACK_L,
	SPRITESTATE_ATTACK_R,
	SPRITESTATE_DEATH
};
static int _id = 0;

class Texture
{
	public:
		Texture();
		Texture(int w, int h, const std::string& _path);
		~Texture();

		void free();

		//sets
		void setW(int wid){width = wid;}
		void setWH(int _width, int _height){
			width = _width;
			height = _height;
		}
		void setSDLTexture(SDL_Texture *t){
			if(texture != NULL)
				SDL_DestroyTexture(texture);
			texture = t;
		}
		void setBlendmode( SDL_BlendMode blending ){SDL_SetTextureBlendMode( texture, blending );}
		void setAlpha( Uint8 alpha ){SDL_SetTextureAlphaMod(texture, alpha);}
		void setColor( Uint8 red, Uint8 green, Uint8 blue ){SDL_SetTextureColorMod( texture, red, green, blue );}

		bool loadClips();
		//gets
		Hitbox& getHitbox(spriteState_t state){return hitboxes[state];}
		SDL_Texture* getTexture(){return texture;}
		SDL_Point* getCenter(){return center;}
		SDL_Rect* getClip(int sprite_state, int current_frame){return sprite_clips[sprite_state].at(current_frame);}
		int getID(){return id;}
		int getWidth(){return width;}
		int getHeight(){return height;}
		int getNumberOfFrames(int sprite_state) {return sprite_clips[sprite_state].size();}
		double getAngle(){return render_angle;}
		bool isAnimated(){return animated;}

	private:
		SDL_Texture* texture;
		SDL_Point* center;
		std::string path;
		std::vector<SDL_Rect*> sprite_clips[NUMBEROF_SPRITESTATES];
		Hitbox hitboxes[NUMBEROF_SPRITESTATES];
		bool animated;
		int id;
		int width;
		int height;
		double render_angle;
};

#endif
