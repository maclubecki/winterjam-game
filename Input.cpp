#include "Input.h"

Input::Input(): clicked(false),pressed_enter(false)
{
}
Input::~Input()
{
}
void Input::getInput(bool &quit, std::string& input_text)
{
	SDL_Event e;

	while( SDL_PollEvent( &e ) )
	{
		if( e.type == SDL_QUIT )
		{
			quit = true;
			break;
		}
		else if( e.type == SDL_MOUSEBUTTONDOWN )
		{			
			SDL_GetMouseState(&mx, &my);
			clicked = true;
		}
		else if( e.type == SDL_KEYDOWN )
		{
			switch(e.key.keysym.sym)
			{
				case SDLK_BACKSPACE: 
					if(!input_text.empty())
						input_text.pop_back();
					break;
				
				case SDLK_RETURN:
					pressed_enter = true;
					break;
			}
		}
		else if( e.type == SDL_TEXTINPUT )
		{			
			input_text += e.text.text;
		}
	}
}
void Input::processInput(Player* hero, std::string& input_text, SDL_Point *camera)
{
	if(pressed_enter){
		hero->triggerShot(input_text);
		input_text.clear();
		pressed_enter = false;
	}
	if(clicked){
		hero->setWaypoint((float)mx,(float)my);
		clicked = false;
	}
}