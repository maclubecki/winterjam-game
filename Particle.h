#ifndef PARTICLE_H
#define PARTICLE_H

#include "Entity.h"

enum particleType_t{
	PARTICLETYPE_SPLASH,
	PARTICLETYPE_SNOW
};

class Particle: public Primitive
{
public:
	Particle(float x, float y, Texture* t, particleType_t type_);
	~Particle(){}

	particleType_t getType() const{return type;}
	void logicUpdate(float accumulator, SDL_Point *camera);

private:
	particleType_t type;
	int interval;
};

#endif