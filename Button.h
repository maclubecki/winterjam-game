#ifndef BUTTON_H
#define BUTTON_H
#include "SDL.h"

class Button{

public:

	Button(int x, int y);
	~Button();
	int X() const {return pos_quad.x;}
	int Y() const {return pos_quad.y;}
	bool isSelected(int mx, int my) const;
	void setClip(bool selected);
	SDL_Rect* getClip() const {return clip;}

private:
	bool selected;
	SDL_Rect pos_quad;
	SDL_Rect* clip;	

};

#endif