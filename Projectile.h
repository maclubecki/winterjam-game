#ifndef PROJECTILE_H
#define PROJECTILE_H

#pragma once
#include "Entity.h"

class Enemy;

class Projectile :
	public Entity
{
public:

	Projectile(float x, float y, Texture* _texture, float targetX_, float targetY_, bool is_player_created_):
		Entity(x,y,_texture), hitTarget(false), is_player_created(is_player_created_)
	{
		if(is_player_created)
			velocity = 300;
		else
			velocity = 120;
		setWaypoint(targetX_, targetY_);
		hitbox = texture->getHitbox(sprite_state);
		e_state = ENTITYSTATE_IDLE;
	}
	~Projectile(){}

	bool hasHitTarget() const {return hitTarget;}
	bool isPlayerCreated() const {return is_player_created;}
	void logicUpdate(float accumulator, SDL_Point *camera = NULL);

private:

	bool hitTarget;
	bool is_player_created;
};

#endif