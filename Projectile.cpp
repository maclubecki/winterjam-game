#include "Projectile.h"
#include "Enemy.h"

void Projectile::logicUpdate(float accumulator, SDL_Point *camera){

	if(destruction_trigger_on) 
		return;
	move(accumulator, camera);	

	if(reachedWaypoint() && !destruction_trigger_on){
		setDestruction(DESTRUCTION_DELAY_PROJECTILE);
	}
}