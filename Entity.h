#ifndef ENTITY_H
#define ENTITY_H

#pragma once
#include "Primitive.h"
#include <list>

enum entityState_t{//death not included because only used once
	ENTITYSTATE_IDLE,
	ENTITYSTATE_RUN,
	ENTITYSTATE_ATTACK,
	ENTITYSTATE_FLINCH,
	ENTITYSTATE_DEATH
};
enum destructionDelay_t{
	DESTRUCTION_DELAY_ENTITY = 100,
	DESTRUCTION_DELAY_ENEMY = 800,
	DESTRUCTION_DELAY_PROJECTILE = 100
};

class Entity : public Primitive
{
public:
	Entity(float x, float y, Texture* _texture) : 
		Primitive(x, y, _texture), wp_active(false),destruction_trigger_on(false), dying(false),e_state(ENTITYSTATE_IDLE) {
			velocity = 100;			
	}
	virtual ~Entity(){}	

	void updateSpriteState();	
	void applyEntitySpriteState(entityState_t es);
	void move(float accumulator, SDL_Point *camera = NULL);
	void setWaypoint(float x, float y);	
	void logicUpdate(float accumulator, SDL_Point *camera);
	void setEntityState(entityState_t es) {e_state = es;}
	bool isDying() const{return dying;}
	bool isDestructionTriggered() const {return destruction_trigger_on;}	
	bool tobeDestroyed() const{
		if(SDL_GetTicks() > destruction_time && destruction_trigger_on)
			return true;
		return false;
	}
	void setDestruction(int delay){
		if(destruction_trigger_on)
			return;
		destruction_trigger_on = true;
		destruction_time = SDL_GetTicks() + delay;
	}

protected:	
	bool wp_active;
	bool destruction_trigger_on;
	bool dying;
	Uint32 destruction_time;
	entityState_t e_state;

	bool reachedWaypoint();
};


#endif