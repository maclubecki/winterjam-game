#include "Particle.h"
#include <cstdlib>

Particle::Particle(float x, float y, Texture* t, particleType_t type_):
	Primitive(x,y,t), type(type_)
{
	switch(type){
	case PARTICLETYPE_SNOW:
		velocity = 80 + rand() % 50;
		setDirX(0.1);
		setDirY(0.5);
		interval = 100 + rand() % 50;
		break;
	case PARTICLETYPE_SPLASH:
		velocity = 0;
		break;
	}
}
void Particle::logicUpdate(float accumulator, SDL_Point *camera){
	if(type != PARTICLETYPE_SNOW)
		return;
	move(accumulator, camera);

	//reset position if out of bounds
	if(getY() > CHUDEJ_RESY){
		coords_now.posX = rand()%CHUDEJ_RESX;
		coords_now.posY = 0;
	}
	if(getX() > CHUDEJ_RESX){
		coords_now.posX = rand()%CHUDEJ_RESX;
	}

}
