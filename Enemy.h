#ifndef ENEMY_H
#define ENEMY_H

#pragma once
#include "Engine.h"
#include "EntityContainer.h"

const int STUN_DURATION = 300;
const int SHOOTER_VELOCITY = 25;

enum enemyType_t{
	ENEMY_ZOMBIE,
	ENEMY_CRAB,
	ENEMY_SHOOTER
};

class Enemy : public Entity
{
public:
	Enemy(float x, float y, const enemyType_t &e_t, const std::string &killword_, Texture* tex, Texture* word_texture);
	~Enemy(void)
	{
		killword_texture->free();
		killword_texture = NULL;
	}

	void sufferDamage();
	void resetWord(Texture* word_texture, const std::string &word);
	void setTargeted(bool t){targeted=t;}
	int getScore() const {return score;}
	int getHP() const {return hp_cur;}
	bool gotHit(){
		if(got_hit){
			got_hit = false;
			return true;
		}
		return false;
	}
	bool isTargeted() const {return targeted;}
	bool isTargetingHero() {
		if(type==ENEMY_SHOOTER && ability_activated){ 
			ability_activated = false;
			return true;
		} 
		return false;
	}
	Texture* getWordTexture() const {return killword_texture;}
	std::string getWord() const {return killword;}

	void logicUpdate(float accumulator, Player* hero,  EntityContainer<Projectile> &p, SDL_Point *camera = NULL);

private:
	bool ability_activated;
	bool targeted;
	bool got_hit;
	int hp_cur, hp_max;
	int score;
	std::string killword;
	enemyType_t type;
	Texture *killword_texture;
	Uint32 ability_activation_time;
	Uint32 stun_time;

	int distanceToTarget(Entity *e) const{
		//get center of hitbox as destination, could have used coords.wp but this function is more flexible
		int tx = e->getX() + e->hbX() + e->hbW()/2;
		int ty = e->getY() + e->hbY() + e->hbH()/2;
		return (int)( sqrt( pow((getX()-tx),2) + pow((getY()-ty),2)) );
	}
};

#endif