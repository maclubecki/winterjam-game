#include "Primitive.h"

Primitive::Primitive(float x, float y, Texture* _texture) :
	frame_current(0), acceleration(0.0), sprite_state(SPRITESTATE_IDLE),
	texture(_texture), texture_clip(NULL),	texture_flip_type(SDL_FLIP_NONE), facing(EFACING_DOWN),
	current_hitbox(SPRITESTATE_IDLE), animation_done(false), moving(false)
{
	coords_init( coords_now, x, y );
	coords_init( coords_prev, x, y );

	looped_animations[SPRITESTATE_IDLE  ] = true;
	looped_animations[SPRITESTATE_RUN_D ] = true;
	looped_animations[SPRITESTATE_RUN_U ] = true;
	looped_animations[SPRITESTATE_RUN_L ] = true;
	looped_animations[SPRITESTATE_RUN_R ] = true;
	looped_animations[SPRITESTATE_FLINCH_D] = false;
	looped_animations[SPRITESTATE_FLINCH_U] = false;
	looped_animations[SPRITESTATE_FLINCH_L] = false;
	looped_animations[SPRITESTATE_FLINCH_R] = false;
	looped_animations[SPRITESTATE_ATTACK_D] = false;
	looped_animations[SPRITESTATE_ATTACK_U] = false;
	looped_animations[SPRITESTATE_ATTACK_L] = false;
	looped_animations[SPRITESTATE_ATTACK_R] = false;
	looped_animations[SPRITESTATE_DEATH] = false;

	if(texture == NULL)
		return;

	if(texture->isAnimated())
		setNextSprite();
}
void Primitive::logicUpdate(float accumulator, SDL_Point *camera){
	move(accumulator,camera);
	setSide();
}
//collision related
void Primitive::collisionResponse(Primitive &b)
{
	solveCollision(b);
}
bool Primitive::intersects(Primitive &b) const
{
	/*
	if( getY() + getHig() > b.getY() )
		if( getY() < b.getY() + b.getHig() )
			if( getX() + getWid() > b.getX() )
				if( getX() < b.getX() + b.getWid() )
					return true;
	return false;
	*/
	if( getY() + hbH() > b.getY() + b.hbY() )
		if( getY() + hbY() < b.getY() + b.hbH() )
			if( getX() + hbW() > b.getX() + b.hbX() )
				if( getX() + hbX() < b.getX() + b.hbW() )
					return true;
	return false;
}
void Primitive::solveCollision(Primitive &b)
{
	float left, right, top, bottom, x, y;

	left = (float)( b.getX() - (getX() + getWid()) );
	right = (float)( (b.getX() + b.getWid()) - getX() );
	top = (float)( b.getY() - (getY() + getHig()) );
	bottom = (float)( (b.getY() + b.getHig()) - getY() );

	// box dont intersect
    if (left > 0 || right < 0) return;
    if (top > 0  || bottom < 0) return;

	if( std::abs(left) < std::abs(right) ) x = left;
	else x = right;

	if( std::abs(top) < std::abs(bottom) ) y = top;
	else y = bottom;

	if( std::abs(x) < std::abs(y) ) y = 0;
	else x = 0;

	setPosition( getX() + x, getY() + y );
}
//movement related
void Primitive::setFacingDir(int tx, int ty){
	const float dx = tx - coords_now.posX;
    const float dy = ty - coords_now.posY;
    const float length = sqrt(dx * dx + dy * dy);
    coords_now.dirX = dx/length;
    coords_now.dirY = dy/length;
}
void Primitive::move( float accumulator, SDL_Point *camera )
{
	moving = false;

	const float distance = velocity * accumulator;

	const float dx = coords_now.dirX * distance;
	coords_prev.posX = coords_now.posX;
	coords_now.posX += dx;
	const float dy = coords_now.dirY * distance;
	coords_prev.posY = coords_now.posY;
	coords_now.posY += dy;
	if(dx != 0 || dy != 0)
		moving = true;

	//camera update
	if(camera != NULL){
		camera->x =  (int)(-coords_now.posX) + 300;//-= dx;
		camera->y -= (int)dy;
	}
}
//animation related
void Primitive::updateSpriteState()
{
	//Flipping sprites
	if( coords_now.dirX > 0 ) texture_flip_type = SDL_FLIP_NONE;
	else if( coords_now.dirX < 0 ) texture_flip_type = SDL_FLIP_HORIZONTAL;
}
void Primitive::setSpriteState(spriteState_t state){
	if( state == sprite_state ) return;
	sprite_state = state;
	animation_done = false;
	frame_current = 0;
	hitbox = texture->getHitbox(sprite_state);
}
void Primitive::setNextSprite()
{
	if(texture->isAnimated() && !looped_animations[sprite_state] && (animationDone() && texture->getNumberOfFrames(sprite_state) != 1))
		return;

	texture_clip = texture->getClip(sprite_state, frame_current);
	++frame_current;
	frame_current %= texture->getNumberOfFrames(sprite_state);
	if(!looped_animations[sprite_state] && frame_current == 0)
		animation_done = true;
	width = texture_clip->w;
	height = texture_clip->h;
}
bool Primitive::animationDone(){
	return animation_done;
}
void Primitive::coords_init(Coordinates &c, float x, float y)
{
	c.posX = x;
	c.posY = y;
	c.wpX = x;
	c.wpY = y;
	c.dirX = 0.0;
	c.dirY = 0.0;
}
