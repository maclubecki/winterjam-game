#include "Game.h"

#include<sstream>
    template <typename T>
    std::string to_string(T value)
    {
      //create an output string stream
      std::ostringstream os ;

      //throw the value into the string stream
      os << value ;

      //convert the string stream into a string and return
      return os.str() ;
    }

bool between(int x, int a, int b){
	if(x >= a && x <= b)
		return true;
	return false;
}
Game::Game():time_last_spawn(0), score(0), gamestate(GAMESTATE_MENU)
{
	//Initialize engine first - SDL and all subsystems
	main_engine = new Engine();
	main_engine->startup();
	wordbase.reserve(150);
	//Load files:
	if( !main_engine->loadMedia(wordbase) )
		throw printf( "Error loading media!" );

	//Init camera
	player_camera = new SDL_Point();
	player_camera->x = 100;
	player_camera->y = 100;

	//Init ui colors:
	//hardcoded lol
	ui.colors[UI_COLOR_INTERFACE].r = 70;
	ui.colors[UI_COLOR_INTERFACE].g = 85;
	ui.colors[UI_COLOR_INTERFACE].b = 94;
	ui.colors[UI_COLOR_INPUT].r = 0;
	ui.colors[UI_COLOR_INPUT].g = 0;
	ui.colors[UI_COLOR_INPUT].b = 0;
	ui.colors[UI_COLOR_STAMBAR].r = 196;
	ui.colors[UI_COLOR_STAMBAR].g = 219;
	ui.colors[UI_COLOR_STAMBAR].b = 181;
	ui.colors[UI_COLOR_FILTER].r = 180;
	ui.colors[UI_COLOR_FILTER].g = 193;
	ui.colors[UI_COLOR_FILTER].b = 214;
	ui.colors[UI_COLOR_SCORE].r = 0;
	ui.colors[UI_COLOR_SCORE].g = 0;
	ui.colors[UI_COLOR_SCORE].b = 0;
	ui.colors[UI_COLOR_ENEMYMAXHP].r = 34;
	ui.colors[UI_COLOR_ENEMYMAXHP].g = 177;
	ui.colors[UI_COLOR_ENEMYMAXHP].b = 76;
	ui.colors[UI_COLOR_ENEMY2HP].r = 255;
	ui.colors[UI_COLOR_ENEMY2HP].g = 128;
	ui.colors[UI_COLOR_ENEMY2HP].b = 0;
	ui.colors[UI_COLOR_ENEMY1HP].r = 255;
	ui.colors[UI_COLOR_ENEMY1HP].g = 20;
	ui.colors[UI_COLOR_ENEMY1HP].b = 20;
	ui.colors[UI_COLOR_ENEMYFRAME].r = 53;
	ui.colors[UI_COLOR_ENEMYFRAME].g = 65;
	ui.colors[UI_COLOR_ENEMYFRAME].b = 79;
	ui.colors[UI_COLOR_SNOW].r = 255;
	ui.colors[UI_COLOR_SNOW].g = 255;
	ui.colors[UI_COLOR_SNOW].b = 255;

	//Init ui textures:
	ui.input_text_texture = new Texture();
	ui.time_text = new Texture();
	main_engine->createRectTexture(ui.stamina_bar_back = new Texture(), ui.colors[UI_COLOR_INTERFACE]);
	ui.stamina_bar_back->setWH(104,11);
	main_engine->createRectTexture(ui.stamina_bar = new Texture(), ui.colors[UI_COLOR_STAMBAR]);
	ui.stamina_bar->setWH(100,7);
	ui.stamina_bar->setColor(196,219,181);
	main_engine->createRectTexture(ui.night_filter = new Texture(), ui.colors[UI_COLOR_FILTER]);
	ui.night_filter->setWH(CHUDEJ_RESX,CHUDEJ_RESY);
	ui.night_filter->setBlendmode(SDL_BLENDMODE_BLEND);
	ui.night_filter->setAlpha(70);
	main_engine->createTextTexture(ui.score_text = new Texture(), to_string(score), ui.colors[UI_COLOR_SCORE]);
	//Initialize player character:
	temp_hero = new Player(CHUDEJ_RESX/2, CHUDEJ_RESY/2, main_engine->getTexture(0));

	//Initialize snow particles:
	particles.reserve(300);
	for(int i = 0; i < 300; ++i){
		Texture* snow = new Texture();
		main_engine->createRectTexture(snow, ui.colors[UI_COLOR_SNOW]);
		snow->setWH(4,4);
		particles.push_back(new Particle(rand()%CHUDEJ_RESX,rand()%CHUDEJ_RESY, snow, PARTICLETYPE_SNOW));
	}

}
Game::~Game()
{
	delete player_camera;
	delete temp_hero;
	temp_hero = NULL;

	for(int i = 0; i < particles.size(); ++i)
		delete particles.at(i);

	//UI textures:
	delete ui.input_text_texture;
	delete ui.stamina_bar;
	delete ui.stamina_bar_back;
	delete ui.night_filter;
	delete ui.score_text;

	//Engine instance:
	delete main_engine;
	main_engine = NULL;
}
void Game::spawnEnemy(){
	srand(SDL_GetTicks());

	//determine spawn cd depending on game progression
	const int time_elapsed = (SDL_GetTicks() - time_game_started)/1000;
	int spawncd = 6000;
	for(int x = time_elapsed; x >= 30; x-=30)
		spawncd -= 250;
	if(spawncd < 1500) spawncd = 1500;

	if(time_last_spawn != 0 && SDL_GetTicks() < time_last_spawn + spawncd)
		return;

	//determine random spawn point
	int x = 0, y = 0;
	int r = rand() % 100;
	const int f = 25;
	if(between(r,0,24)){//left
		x = -f;
		y = rand()%CHUDEJ_RESY;
	}
	else if(between(r,25,49)){//right
		x = CHUDEJ_RESX+f;
		y = rand()%CHUDEJ_RESY;
	}
	else if(between(r,50,74)){//up
		x = rand()%CHUDEJ_RESX;
		y = -f;
	}
	else if(between(r,75,100)){//down
		x = rand()%CHUDEJ_RESX;
		y = CHUDEJ_RESY+f;
	}

	//determine enemy type
	r = rand() % 100;
	std::string word = wordbase.at(rand()%wordbase.size());
	Texture* word_texture = new Texture();
	main_engine->createFramedTextTexture(word_texture, word, ui.colors[UI_COLOR_ENEMYFRAME], ui.colors[UI_COLOR_ENEMYMAXHP]);
	if(between(r,0,44))			ec_enemies.push(new Enemy(x,y,ENEMY_ZOMBIE, word, main_engine->getTexture(1), word_texture));
	else if(between(r,45,79))	ec_enemies.push(new Enemy(x,y,ENEMY_CRAB, word, main_engine->getTexture(2), word_texture));
	else						ec_enemies.push(new Enemy(x,y,ENEMY_SHOOTER, word, main_engine->getTexture(3), word_texture));

	time_last_spawn = SDL_GetTicks();
}
void Game::executeLogic(const float accumulator){
	if(temp_hero->isDying()){
		gamestate = GAMESTATE_OVER;
		return;
	}

	//bullet updates
	for (std::list<Projectile*>::iterator iter = ec_projs.begin(); iter != ec_projs.end(); ++iter){
		(*iter)->logicUpdate(accumulator, player_camera);

		if((*iter)->tobeDestroyed()){
			ec_projs.pushTrash(iter);
		}
		if(!(*iter)->isPlayerCreated() && (*iter)->intersects(*temp_hero)){
			temp_hero->triggerDeath();
		}
	}
	//enemy updates
	for(std::list<Enemy*>::iterator iter = ec_enemies.begin(); iter!=ec_enemies.end(); ++iter){
		(*iter)->logicUpdate(accumulator, temp_hero, ec_projs, player_camera);

		if((*iter)->isTargetingHero()){
			Projectile* bullet = new Projectile((*iter)->getXCenter(), (*iter)->getYCenter(), main_engine->getTexture(10), temp_hero->getXCenter(), temp_hero->getYCenter(), false);
			ec_projs.push(bullet);
		}
		if((*iter)->isTargeted()){
			temp_hero->setEntityState(ENTITYSTATE_ATTACK);
			temp_hero->setFacingDir((*iter)->getXCenter(), (*iter)->getYCenter());
			Projectile* snowball = new Projectile(temp_hero->getXCenter(), temp_hero->getYCenter(), main_engine->getTexture(5), (*iter)->getXCenter(), (*iter)->getYCenter(), true);
			ec_projs.push(snowball);
			(*iter)->setTargeted(false);
		}
		if((*iter)->gotHit() && !(*iter)->isDying()){
			Texture* new_word_t = new Texture();
			std::string new_word = wordbase.at(rand()%wordbase.size());
			if((*iter)->getHP() == 2)
				main_engine->createFramedTextTexture(new_word_t, new_word, ui.colors[UI_COLOR_ENEMYFRAME], ui.colors[UI_COLOR_ENEMY2HP]);
			else
				main_engine->createFramedTextTexture(new_word_t, new_word, ui.colors[UI_COLOR_ENEMYFRAME], ui.colors[UI_COLOR_ENEMY1HP]);
			(*iter)->resetWord(new_word_t, new_word);
		}
		if((*iter)->tobeDestroyed()){
			score+= (*iter)->getScore();
			ec_enemies.pushTrash(iter);
		}
	}
	//particle updates
	for(int i=0; i < particles.size(); ++i)
		particles.at(i)->logicUpdate(accumulator, player_camera);


	//hero update
	temp_hero->logicUpdate(accumulator,player_camera);

	spawnEnemy();
	//timer update
	timer_s = (SDL_GetTicks() - time_game_started) / 1000;
	timer_m = timer_s / 60;
	timer_s %= 60;
	//cleanup
	ec_projs.cleanTrash();
	ec_enemies.cleanTrash();
}
void Game::spriteUpdates(int framecount){
	for(std::list<Enemy*>::iterator iter = ec_enemies.begin(); iter!=ec_enemies.end(); ++iter){
		(*iter)->updateSpriteState();
		if(framecount%10 == 0)
			(*iter)->setNextSprite();
	}
	for(std::list<Projectile*>::iterator iter = ec_projs.begin(); iter!=ec_projs.end(); ++iter)
		(*iter)->updateSpriteState();

	temp_hero->updateSpriteState();
	if( framecount%10 == 0 )
		temp_hero->setNextSprite();

	//update stamina bar
	if(temp_hero->isExhausted()) ui.stamina_bar->setColor(255,0,0);
	else						 ui.stamina_bar->setColor(196,219,181);
	ui.stamina_bar->setW(temp_hero->getStamina()/2);
	//update text textures
	main_engine->createTextTexture(ui.input_text_texture, input_text, ui.colors[UI_COLOR_INPUT]);
	main_engine->createTextTexture(ui.score_text, "Score: " + to_string(score), ui.colors[UI_COLOR_SCORE]);
	std::string time_str = "Time: " + to_string(timer_m) + ":";
	if(timer_s < 10) time_str += "0";
	time_str += to_string(timer_s);
	main_engine->createTextTexture(ui.time_text, time_str ,ui.colors[UI_COLOR_SCORE]);
}
void Game::drawWorld(){
	main_engine->screenClear();

	//background
	main_engine->renderTexture(main_engine->getTexture(4),0,0);

	for(std::list<Enemy*>::iterator iter = ec_enemies.begin(); iter!=ec_enemies.end(); ++iter)
		main_engine->renderEnemy(*iter);
	for (std::list<Projectile*>::iterator iter = ec_projs.begin(); iter != ec_projs.end(); ++iter)
		main_engine->renderPrimitive(*iter);

	main_engine->renderPrimitive(temp_hero);

	for(int i=0; i < particles.size(); ++i)
		main_engine->renderPrimitive(particles.at(i));

	main_engine->renderTexture(ui.night_filter,0,0);

	const int bottom = CHUDEJ_RESY - main_engine->getFontSize();
	main_engine->renderTexture(ui.input_text_texture,temp_hero->getXCenter() - ui.input_text_texture->getWidth()/2, temp_hero->getY() - 18);
	main_engine->renderTexture(ui.stamina_bar_back,3,bottom-42);
	main_engine->renderTexture(ui.stamina_bar,5,bottom-40);
	main_engine->renderTexture(ui.time_text,5,bottom-ui.score_text->getHeight() - 5);
	main_engine->renderTexture(ui.score_text,5,bottom - 2);
	main_engine->screenUpdate();
}
void Game::reset(){
	//reset hero
	temp_hero->reset();
	//clear containers
	ec_enemies.clearContainers();
	ec_projs.clearContainers();
	//game variables
	delete ui.score_text;
	ui.score_text = new Texture();
	input_text.clear();
	delete ui.input_text_texture;
	ui.input_text_texture = new Texture();
	time_last_spawn = 0;
	score = 0;
	timer_m = 0;
	timer_s = 0;
	gamestate = GAMESTATE_RUN;
}
void Game::run(bool &quit)
{
	Uint32 time_old, time_new;
    float time_frame;
    float accumulator = 0.0;
	int framecount = 0;
    const float dt = 0.01f;

    time_old = time_game_started = SDL_GetTicks();
    srand(SDL_GetTicks());

    while(!quit)
	{
		time_new = SDL_GetTicks();
        time_frame = ( time_new - time_old )/1000.f;
		if( time_frame > 0.3 ) time_frame = 0.3f;
        time_old = time_new;
		int avg_fps = static_cast<int>( framecount/(time_new/1000.f) );
        accumulator += time_frame;

		//All logic goes here:
		while( accumulator >= dt )
		{
			main_engine->processInput(quit, temp_hero, input_text, player_camera);
			executeLogic(accumulator);
			accumulator -= dt;
		}
		//Update sprites:
		spriteUpdates(framecount);
		drawWorld();

		++framecount;
		//printf("FPS: %d\n", avg_fps);
		if(gamestate == GAMESTATE_OVER)
			break;
	}
}
void Game::startMenu(bool &quit)
{
	Button startBtn(CHUDEJ_RESX/2 - 70,450), quitBtn(CHUDEJ_RESX/2 - 70,500);
    SDL_Event e;
	int x = 0, y = 0;
	Uint32 time_old, time_new;
    float time_frame;
    float accumulator = 0.0;
    const float dt = 0.01f;

    time_old = time_game_started = SDL_GetTicks();

	while(!quit){
		time_new = SDL_GetTicks();
        time_frame = ( time_new - time_old )/1000.f;
		if( time_frame > 0.3 ) time_frame = 0.3f;
        time_old = time_new;
        accumulator += time_frame;

		if( SDL_PollEvent(&e)){
			x = e.motion.x;
			y = e.motion.y;
			if( e.type == SDL_MOUSEBUTTONDOWN )	{
				if( e.button.button == SDL_BUTTON_LEFT ){
					if(startBtn.isSelected(x,y)){
						gamestate = GAMESTATE_RUN;
						break;
					}
					if(quitBtn.isSelected(x,y)){
						quit = true;
						return;
					}
				}
			}
			if(e.type == SDL_QUIT ){
				quit = true;
				return;
			}
		}

		main_engine->screenClear();
		main_engine->renderTexture(main_engine->getTexture(4),0,0);

		while( accumulator >= dt )
		{
			startBtn.setClip(startBtn.isSelected(x,y));
			quitBtn.setClip(quitBtn.isSelected(x,y));
			for(int i=0; i < particles.size(); ++i){
				particles.at(i)->logicUpdate(accumulator, player_camera);
				main_engine->renderPrimitive(particles.at(i));
			}
			accumulator -= dt;
		}

		//splash
		main_engine->renderTexture(main_engine->getTexture(9), CHUDEJ_RESX/2 - main_engine->getTexture(9)->getWidth()/2, 10);
		main_engine->renderTexture(main_engine->getTexture(6), startBtn.X(), startBtn.Y(),startBtn.getClip());
		main_engine->renderTexture(main_engine->getTexture(7), quitBtn.X(), quitBtn.Y(),quitBtn.getClip());
		main_engine->screenUpdate();
	}
}
void Game::gameover(bool &quit){
	int x = 0, y = 0;
	Button resBtn(CHUDEJ_RESX/2 - 70, 425);
	Texture *end_text = new Texture();
	std::string end_string = "You lasted " + to_string(timer_m) + " minutes and " +
		to_string(timer_s) + " seconds, scoring " + to_string(score) + " points!";
	main_engine->createFramedTextTexture(end_text, end_string, ui.colors[UI_COLOR_ENEMYFRAME], ui.colors[UI_COLOR_ENEMYMAXHP]);
    SDL_Event e;

	while(!quit)
	{
		if( SDL_PollEvent(&e)){
			x = e.motion.x;
			y = e.motion.y;
			if( e.type == SDL_MOUSEBUTTONDOWN )	{
				if( e.button.button == SDL_BUTTON_LEFT ){
					if(resBtn.isSelected(x,y)){
						reset();
						break;
					}
				}
			}
			if(e.type == SDL_QUIT ){
				quit = true;
				return;
			}
		}
		resBtn.setClip(resBtn.isSelected(x,y));

		main_engine->screenClear();
		main_engine->renderTexture(main_engine->getTexture(8), resBtn.X(), resBtn.Y(),resBtn.getClip());
		main_engine->renderTexture(end_text, CHUDEJ_RESX/2 - end_text->getWidth()/2, 100);
		main_engine->screenUpdate();
	}

	delete end_text;
}
void Game::mainloop(){
	bool quit = false;

	while(!quit){
		if(gamestate == GAMESTATE_MENU)
			startMenu(quit);
		else if(gamestate == GAMESTATE_RUN)
			run(quit);
		else if(gamestate == GAMESTATE_OVER)
			gameover(quit);
	}
}
