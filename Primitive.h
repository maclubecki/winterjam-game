#ifndef PRIMITIVE_H
#define PRIMITIVE_H
#pragma once

#include "Texture.h"
#include <cmath>

enum entityFacing_t{
	EFACING_UP,
	EFACING_DOWN,
	EFACING_LEFT,
	EFACING_RIGHT
};
struct Coordinates
{
	float posX, posY;
	float wpX, wpY; //destination coordinates
	float dirX, dirY;
};

class Primitive
{
public:

	Primitive(float x, float y, Texture* _texture);
	virtual ~Primitive(){}

	//gets
	float getXCenter() const{return coords_now.posX + width/2;}
	float getYCenter() const{return coords_now.posY + height/2;}
	int hbY() const {return hitbox.y;}
	int hbH() const {return hitbox.h;}
	int hbX() const {return hitbox.x;}
	int hbW() const {return hitbox.w;}
	int getX() const {return (int)coords_now.posX;}
	int getY() const {return (int)coords_now.posY;}
	int getRight() const {return (int)(coords_now.posX + width);}
	int getBottom() const {return (int)(coords_now.posY + height);}
	int getV() const {return velocity;}
	int getWid() const {return width;}
	int getHig()const {return height;}
	float getDirY() const {return coords_now.dirY;}
	Texture* getTexture(){return texture;}

	//sets
	void setDirX(float _dirX) {coords_now.dirX = _dirX;}
	void setDirY(float _dirY) {coords_now.dirY = _dirY;}
	void setPosition(float _x, float _y){
		coords_now.posX = _x;
		coords_now.posY = _y;
	}
	void setTexture(Texture *t){
		texture = t;
		texture->setWH(t->getWidth(), t->getHeight());
	}
	void setFacingDir(int tx, int ty);
	//animation related
	void setNextSprite();
	void setSpriteState(spriteState_t state);
	bool animationDone();
	virtual void updateSpriteState();
	SDL_Rect* getTextureClip(){return texture_clip;}
	SDL_RendererFlip getTextureFlipType(){return texture_flip_type;}
	//collision related (+ 1 protected)
	bool inBounds() const {
		if(getX() > 0 && getX() < CHUDEJ_RESX && getY() > 0 && getY() < CHUDEJ_RESY)
			return true;
		return false;
		}
	bool intersects (Primitive &b) const;
	virtual void collisionResponse(Primitive &b);
	//movement related
	virtual void move(float accumulator, SDL_Point *camera = NULL);
	virtual void logicUpdate(float accumulator, SDL_Point *camera = NULL);

protected:
	Coordinates coords_now;
	Coordinates coords_prev;
	int width, height;
	unsigned velocity;
	unsigned frame_current;
	double acceleration;
	bool moving;
	bool animation_done;
	bool looped_animations[NUMBEROF_SPRITESTATES];
	spriteState_t sprite_state;
	Texture* texture;
	SDL_Rect* texture_clip;
	SDL_RendererFlip texture_flip_type;
	entityFacing_t facing;
	unsigned current_hitbox;
	Hitbox hitbox;

	void coords_init( Coordinates &c, float x, float y );
	virtual void solveCollision(Primitive &b);

	void setSide(){
		if(std::abs(coords_now.dirX) > std::abs(coords_now.dirY)) {
			if(coords_now.dirX < 0)	facing = EFACING_LEFT;
			else					facing = EFACING_RIGHT;
		}
		else {
			if(coords_now.dirY < 0)	facing = EFACING_UP;
			else					facing = EFACING_DOWN;
		}
	}

};

#endif
