#ifndef ENTITYCONTAINER_H
#define ENTITYCONTAINER_H
#include <vector>
#include <list>

template <typename T> 
class EntityContainer
{
public:
	EntityContainer<T>(){
		trash.reserve(10);
	}
	~EntityContainer<T>(){
		clearContainers();
	}
	void clearContainers(){
		trash.clear();
		while(!items.empty()){
			delete (*items.begin());
			items.erase(items.begin());
		}
	}
	void cleanTrash(){		
		for(unsigned i = 0; i < trash.size(); ++i){			
			delete (*trash.at(i));
			items.erase(trash.at(i));
		}
		trash.clear();
	}	
	void push(T *e){items.push_back(e);}
	void pushTrash(typename std::list<T*>::iterator& it){trash.push_back(it);}
	bool empty(){return items.empty();}
	typename std::list<T*>::iterator begin(){return items.begin();}
	typename std::list<T*>::iterator end(){return items.end();}
	std::list<T*>& data(){return items;}

private:
	std::list<T*> items;
	std::vector<typename std::list<T*>::iterator> trash; 
};
#endif