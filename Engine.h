#ifndef ENGINE_H
#define ENGINE_H

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <list>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>
#include "Texture.h"
#include "Input.h"
#include "Player.h"
#include "Projectile.h"
#include "Enemy.h"
#include "EntityContainer.h"
#include "Particle.h"

class Enemy;

class Engine
{
	public:
		Engine(): res_x(CHUDEJ_RESX), res_y(CHUDEJ_RESY), eng_window(NULL), eng_renderer(NULL){}
		~Engine();
		int getFontSize();
		bool startup();
		bool loadMedia(std::vector<std::string> &wordbase);		
		Texture* loadTexture(const std::string &path);
		Texture* getTexture(int id);
		void renderTexture(Texture* rt, int pos_x, int pos_y, SDL_Rect* clip=NULL, SDL_RendererFlip flip_type=SDL_FLIP_NONE);
		void renderPrimitive(Primitive* p, SDL_Point *player_cam = NULL);
		void renderEnemy(Enemy *e, SDL_Point *player_cam=NULL);
		void createFramedTextTexture(Texture* t, const std::string &text, const SDL_Color &framecolor, const SDL_Color &textcolor);
		void createTextTexture(Texture* t, const std::string &text, const SDL_Color &color);
		void createRectTexture(Texture* t, const SDL_Color &c);
		void screenClear();
		void screenUpdate();
		void processInput(bool &quit, Player* hero, std::string& input_text, SDL_Point *player_camera);

	private:
		int res_x, res_y;
		SDL_Window* eng_window;
		SDL_Renderer* eng_renderer;
		TTF_Font* text_font;
		std::list <Texture*> texture_list;
		Input* input_mgr;
};

#endif