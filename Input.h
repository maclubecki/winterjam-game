#ifndef INPUT_H
#define INPUT_H

#include "SDL.h"
#include "Player.h"

class Input
{
public:
	Input();
	~Input();
	void getInput(bool &quit, std::string& input_text);
	void processInput(Player* hero, std::string& input_text, SDL_Point *camera);

private:	
	int mx, my;
	bool pressed_enter;
	bool clicked;
};

#endif