#include "Entity.h"

bool Entity::reachedWaypoint()
{
	const float WP_RADIUS = 1.0;
	if(std::abs(coords_now.posX - coords_now.wpX) < WP_RADIUS && std::abs(coords_now.posY - coords_now.wpY) < WP_RADIUS){
		e_state = ENTITYSTATE_IDLE;
		moving = false;
		return true;
	}
    return false;
}
void Entity::logicUpdate(float accumulator, SDL_Point *camera)
{
	if(destruction_trigger_on)
		return;

	moving = false;
	move(accumulator, camera);
	setSide();
}
void Entity::setWaypoint(float x, float y)
{
	//set wp on the center
	coords_prev.wpX = coords_now.wpX;
	coords_prev.wpY = coords_now.wpY;
	coords_now.wpX = x - width/2;
	coords_now.wpY = y - height/2;
	//current becomes old
    coords_prev.dirX = coords_now.dirX;
    coords_prev.dirY = coords_now.dirY;
	//calculate deltas and path length
    const float dx = coords_now.wpX - coords_now.posX;
    const float dy = coords_now.wpY - coords_now.posY;
    const float length = sqrt(dx * dx + dy * dy);
	//normalize
    coords_now.dirX = dx/length;
    coords_now.dirY = dy/length;
	if(e_state != ENTITYSTATE_ATTACK && e_state != ENTITYSTATE_FLINCH)
		e_state = ENTITYSTATE_RUN;
	wp_active = true;
}
void Entity::move( float accumulator, SDL_Point *camera )
{
	if( !wp_active ) return;

	//current becomes old
    coords_prev.dirX = coords_now.dirX;
    coords_prev.dirY = coords_now.dirY;
	//calculate deltas and path length
    const float dx = coords_now.wpX - coords_now.posX;
    const float dy = coords_now.wpY - coords_now.posY;
    const float length = sqrt(dx * dx + dy * dy);
	if(dx != 0 || dy != 0) moving = true;
	//normalize
    coords_now.dirX = dx/length;
    coords_now.dirY = dy/length;

    coords_prev.posX = coords_now.posX;
	coords_prev.posY = coords_now.posY;
    coords_now.posX += coords_now.dirX * velocity * accumulator;
	coords_now.posY += coords_now.dirY * velocity * accumulator;

	if(reachedWaypoint())
    {
		//keep old coords
		coords_prev.posX = coords_now.posX;
        coords_prev.posY = coords_now.posY;
		coords_prev.dirX = coords_now.dirX;
        coords_prev.dirY = coords_now.dirY;
        wp_active = false;
		moving = false;
    }

	//camera update
	if(camera != NULL){
		camera->x =  (int)(-coords_now.posX) + 300;//-= dx;
		camera->y -= (int)dy;
	}
}
void Entity::updateSpriteState()
{
	//update spritestate based on direction facing
	if(dying || destruction_trigger_on)
		return;

	if( (!moving && e_state != ENTITYSTATE_ATTACK) || e_state == ENTITYSTATE_IDLE) {
		setSpriteState(SPRITESTATE_IDLE );
		return;
	}

	applyEntitySpriteState(e_state);
}
void Entity::applyEntitySpriteState(entityState_t es){
	//ugly but necessary; for easy changing of spritestates without having to use switch every time
	switch(es){
	case ENTITYSTATE_ATTACK:
		switch(facing){
		case EFACING_LEFT:
			setSpriteState(SPRITESTATE_ATTACK_L);
			break;
		case EFACING_RIGHT:
			setSpriteState(SPRITESTATE_ATTACK_R);
			break;
		case EFACING_UP:
			setSpriteState(SPRITESTATE_ATTACK_U);
			break;
		case EFACING_DOWN:
			setSpriteState(SPRITESTATE_ATTACK_D);
			break;
		default:
			setSpriteState(SPRITESTATE_ATTACK_D);
			break;
		}
		break;

	case ENTITYSTATE_RUN:
		switch(facing){
		case EFACING_LEFT:
			setSpriteState(SPRITESTATE_RUN_L);
			break;
		case EFACING_RIGHT:
			setSpriteState(SPRITESTATE_RUN_R);
			break;
		case EFACING_UP:
			setSpriteState(SPRITESTATE_RUN_U);
			break;
		case EFACING_DOWN:
			setSpriteState(SPRITESTATE_RUN_D);
			break;
		default:
			setSpriteState(SPRITESTATE_RUN_D);
			break;
		}
		break;

	case ENTITYSTATE_FLINCH:
		switch(facing){
		case EFACING_LEFT:
			setSpriteState(SPRITESTATE_FLINCH_L);
			break;
		case EFACING_RIGHT:
			setSpriteState(SPRITESTATE_FLINCH_R);
			break;
		case EFACING_UP:
			setSpriteState(SPRITESTATE_FLINCH_U);
			break;
		case EFACING_DOWN:
			setSpriteState(SPRITESTATE_FLINCH_D);
			break;
		default:
			setSpriteState(SPRITESTATE_FLINCH_D);
			break;
		}
		break;
	}
}
