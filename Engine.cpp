#include "Engine.h"

Engine::~Engine()
{
	//Textures:
	while(!texture_list.empty()){
		delete (*texture_list.begin());
		texture_list.erase(texture_list.begin());
	}

	delete input_mgr;

	//Destroy window and renderer
	SDL_DestroyRenderer( eng_renderer );
	SDL_DestroyWindow( eng_window );
	eng_window = NULL;
	eng_renderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}

bool Engine::startup()
{
	//Initializing SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
		throw printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		return false;
	}

	//Initializing SDL_TTF
	if( TTF_Init() == -1 ) { 
		printf( "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError() ); 
		return false; 
	}

	//Set texture filtering to linear
	if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
	{
		printf( "Warning: Linear texture filtering not enabled!" );
	}

	//Creating window
	eng_window = SDL_CreateWindow( "Cold Wordz", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, res_x, res_y, SDL_WINDOW_SHOWN );
	if( eng_window == NULL )
	{
		throw printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
		return false;
	}

	//Create renderer for window
	//might want optional V-Sync?
	eng_renderer = SDL_CreateRenderer( eng_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
	if( eng_renderer == NULL )
	{
		throw printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
		return false;
	}
			
	//Initialize renderer color
	SDL_SetRenderDrawColor( eng_renderer, 0,0,0, 0xFF );

	//Initialize PNG loading
	int imgFlags = IMG_INIT_PNG;
	if( !( IMG_Init( imgFlags ) & imgFlags ) )
	{
		throw printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
		return false;
	}

	//Create input manager
	input_mgr = new Input();

	return true;			
}
Texture* Engine::loadTexture(const std::string &path )
{
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
	if( loadedSurface == NULL )	
		throw printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );	
	
	//Color key image
	SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB( loadedSurface->format, 0, 0xFF, 0xFF ) );

	//Create texture from surface pixels
    newTexture = SDL_CreateTextureFromSurface( eng_renderer, loadedSurface );
	if( newTexture == NULL )
	{
		throw printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
	}
	//Get image dimensions
	int w = loadedSurface->w;
	int h = loadedSurface->h;	

	//Get rid of old loaded surface
	SDL_FreeSurface( loadedSurface );	

	//Create the texture object and load clips from file
	Texture* tex = new Texture(w, h, path);
	tex->setSDLTexture(newTexture);
	tex->loadClips(); 

	//Push the texture to the list and return it outside
	texture_list.push_back( tex );
	return tex;
}

bool Engine::loadMedia(std::vector<std::string> &wordbase)
{
	//Loading textures
	std::ifstream input_file("textures.cfg");
	if(!input_file.is_open()){
		throw printf("Can't find textures configuration file.");
		return false;
	}

    std::string s;
	int iterator = 0;

    while( !input_file.eof() )
    {
        getline(input_file, s);

        //pomijamy linijki puste, wyznaczajace kategorie (np [Video]) lub ewentualne komentarze (zaczynajace sie od ; w pliku)
        if( s[0] == ';' || s[0] == '[' || s.empty() )
            continue;

        if( loadTexture(s) == NULL )
		{
			throw printf("Failed to load texture for %s", &s);
			return false;
		}
		else std::cout << "Loaded " << s << std::endl;
		iterator++;
    }

	input_file.close();

	//Loading font
	text_font = TTF_OpenFont("./AniTypewriter.ttf", 20);
	if(text_font == NULL) { 
		throw printf("Failed to load font! SDL_ttf Error: %s\n", TTF_GetError()); 
		return false; 
	}

	//Loading words
	input_file.open("wordbase.txt");
	if(!input_file.good()){
		throw printf("Can't find wordbase file.");
		return false;
	}
	while(!input_file.eof())
    {
        getline(input_file, s);
		wordbase.push_back(s);
    }

	return true;
}

Texture* Engine::getTexture( int id )
{
	for ( std::list<Texture*>::iterator iter = texture_list.begin(); iter != texture_list.end(); iter++ )
	{       			
		if((*iter)->getID() == id )
			return *iter;
	}
	return NULL;
}
void Engine::createRectTexture(Texture* t, const SDL_Color &c){
	if(t != NULL && t->getTexture() != NULL)
		t->free();

	SDL_Surface* s;
	s = SDL_CreateRGBSurface(0, 1, 1, 32,0,0,0,0);
	if (s == NULL) {
		throw printf("CreateRGBSurface failed: %s\n", SDL_GetError());
		exit(1);
	}

	SDL_FillRect(s, NULL, SDL_MapRGB(s->format, c.r, c.g, c.b));

	SDL_Texture* texture = SDL_CreateTextureFromSurface(eng_renderer, s);
	if (texture == NULL) {
		throw printf("CreateTextureFromSurface failed: %s\n", SDL_GetError());
		exit(1);
	}

	t->setSDLTexture(texture);
	t->setAlpha(c.a);
	t->setWH(1,1);

	SDL_FreeSurface(s);
}
void Engine::createTextTexture(Texture* t, const std::string &text, const SDL_Color &color){
	SDL_Surface* textSurface;
	if(text == ""){
		std::string s = " ";
		textSurface = TTF_RenderText_Blended(text_font, s.c_str(), color);
	}
	else
		textSurface = TTF_RenderText_Blended(text_font, text.c_str(), color);

	if( textSurface == NULL )
		throw printf( "Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError() );
	if(t->getTexture() != NULL)
		t->free();

	t->setSDLTexture(SDL_CreateTextureFromSurface(eng_renderer, textSurface));
	//t->setWH(textSurface->w, textSurface->h);

	if(t->getTexture() == NULL) 	
		throw printf( "Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError() );
	else 
		t->setWH(textSurface->w, textSurface->h);

	SDL_FreeSurface( textSurface );
}
void Engine::createFramedTextTexture(Texture* t, const std::string &text, const SDL_Color &framecolor, const SDL_Color &textcolor){
	SDL_Surface *destination, *textSurface;
	//create text surface
	if(text == ""){
		std::string s = " ";
		textSurface = TTF_RenderText_Blended(text_font, s.c_str(), textcolor);
	}
	else textSurface = TTF_RenderText_Blended(text_font, text.c_str(), textcolor);
	if( textSurface == NULL )
		throw printf( "Unable to render text surface for text framed surface! SDL_ttf Error: %s\n", TTF_GetError() );

	//create rect surface
	destination = SDL_CreateRGBSurface(0, textSurface->w + 4, textSurface->h + 4, 32,0,0,0,0);
	if (destination == NULL) {
		throw printf("CreateRGBSurface failed: %s\n", SDL_GetError());
		exit(1);
	}
	SDL_FillRect(destination, NULL, SDL_MapRGB(destination->format, framecolor.r, framecolor.g, framecolor.b));
	SDL_Rect* destination_clip = new SDL_Rect();
	destination_clip->x = 2;
	destination_clip->y = 2;
	destination_clip->w = destination->w;
	destination_clip->h = destination->h;
	SDL_BlitSurface(textSurface, NULL, destination, destination_clip);

	if(t->getTexture() != NULL)
		t->free();

	//create texture
	t->setSDLTexture(SDL_CreateTextureFromSurface(eng_renderer, destination));
	if(t->getTexture() == NULL) 	
		throw printf( "Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError() );
	else 
		t->setWH(destination->w, destination->h);

	SDL_FreeSurface(textSurface);
	SDL_FreeSurface(destination);
	delete destination_clip;
}
void Engine::renderTexture(Texture* rt, int pos_x, int pos_y, SDL_Rect* clip, SDL_RendererFlip flip_type)
{
	SDL_Rect renderQuad = { pos_x, pos_y, rt->getWidth(), rt->getHeight() };

	if(clip != NULL){
		renderQuad.w = clip->w; 
		renderQuad.h = clip->h;
	}

	SDL_RenderCopyEx( eng_renderer, rt->getTexture(), clip, &renderQuad, rt->getAngle(), rt->getCenter(), flip_type);
}
void Engine::renderPrimitive(Primitive* p, SDL_Point *player_cam)
{
	if(p->getTexture() == NULL){
		throw printf("Trying to render a primitive without a texture");
		return;
	}

	if(player_cam != NULL)
		renderTexture(p->getTexture(), p->getX() + player_cam->x, p->getY() + player_cam->y, p->getTextureClip(), p->getTextureFlipType() );
	else
		renderTexture(p->getTexture(), p->getX(), p->getY(), p->getTextureClip(), p->getTextureFlipType() );
}
void Engine::renderEnemy(Enemy *e, SDL_Point *player_cam){
	if(e->getTexture() == NULL){
		throw printf("Trying to render a primitive without a texture");
		return;
	}	
	renderPrimitive(e,player_cam);
	renderTexture(e->getWordTexture(), e->getXCenter() - e->getWordTexture()->getWidth()/2, e->getY() - 30);
}
void Engine::screenClear()
{
	SDL_RenderClear( eng_renderer );
}
void Engine::screenUpdate()
{
	SDL_RenderPresent( eng_renderer );
}
void Engine::processInput(bool &quit, Player* hero, std::string& input_text, SDL_Point *player_camera )
{
	input_mgr->getInput(quit,input_text);
	if(quit) return;
	input_mgr->processInput(hero, input_text, player_camera);
}
int Engine::getFontSize(){
	return TTF_FontHeight(text_font);
}