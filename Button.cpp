#include "Button.h"

Button::Button(int x, int y):
	selected(false)
{
	pos_quad.x = x;
	pos_quad.y = y;
	pos_quad.w = 140;
	pos_quad.h = 40;
	clip = new SDL_Rect();
	clip->x = 0;
	clip->w = 140;
	clip->h = 40;
	setClip(true);
}
Button::~Button(){
	delete clip;
}
void Button::setClip(bool selected){
	if(!selected) clip->y = 0;	
	else		 clip->y = 40;
}
bool Button::isSelected(int mx, int my) const {
	if(mx >= pos_quad.x && mx <= pos_quad.x + pos_quad.w && my >= pos_quad.y && my <= pos_quad.y + pos_quad.h)
		return true;
	return false;
}
