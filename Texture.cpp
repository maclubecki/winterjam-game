#include "Texture.h"
#include <fstream>

Texture::Texture():
	id(_id++), texture(NULL), width(0), height(0), path(""), render_angle(0.0),
	center(NULL), animated(false)
{
}
Texture::Texture(int w, int h, const std::string& _path):
	id(_id++), texture(NULL), width(w), height(h), path(_path), render_angle(0.0),
	center(NULL), animated(false)
{
}

Texture::~Texture()
{
	//freeing texture
	free();
	//clearing vector of clips
	for(int i = 0; i < 5; i++)
	{
		for( unsigned j = 0; j < sprite_clips[i].size(); j++)
			delete sprite_clips[i].at(j);
		sprite_clips[i].clear();
	}
}

void Texture::free()
{
	//Free texture if it exists
	if( texture != NULL )
	{
		SDL_DestroyTexture( texture );
		texture = NULL;
		width = 0;
		height = 0;
	}
}

SDL_Rect* splitByComma( const std::string &s )
{
    std::string sub;
    unsigned int start = 0, end = 0;
    int tab[4];

    for(int i = 0; i < 4; i++ )
    {
        for( end = start; end < s.length(); end++ )
            if( s[end] == ',') break;

        if( i == 3 ) end = s.length()-1;

        sub = s.substr(start, end);
        tab[i] = std::atoi( sub.c_str() );
        start = end+1;
    }

    SDL_Rect* r = new SDL_Rect();
    r->x = tab[0];
    r->y = tab[1];
    r->w = tab[2];
    r->h = tab[3];

    return r;
}
void hsplitByComma( const std::string &s, Hitbox& hb )
{
    std::string sub;
    unsigned int start = 0, end = 0;
    int tab[4];

    for(int i = 0; i < 4; i++ )
    {
        for( end = start; end < s.length(); end++ )
            if( s[end] == ',') break;

        if( i == 3 ) end = s.length()-1;

        sub = s.substr(start, end);
        tab[i] = atoi( sub.c_str() );
        start = end+1;
    }

	hb.x = tab[0];
    hb.y = tab[1];
    hb.w = tab[2];
    hb.h = tab[3];
}

bool Texture::loadClips()
{
	std::ifstream input_file("clips.cfg");
    std::string s;
	bool completed = false;
	bool foundPath = false;
	bool foundHitbox = false;
	int foundStates = 0;
	int foundHitboxes = 0;
	int foundXYWHs = 0;
	int curState;

    while( !input_file.eof() || completed )
    {
        getline(input_file, s);

        if( s[0] == ';' || s[0] == '[' || s.empty() )
            continue;

		//look for path if not found yet
        if(s.substr(0,5) == "PATH="){
			if(foundPath)
				return true;
            //erase the 'path=' substring
            s.erase(0,5);
            if( s == path ){
				animated = true;
                foundPath = true;
			}
            else
                continue;
        }
        //look for sprite states if found the path
        if(s.substr(0,6) == "STATE=" && foundPath){
            //store the state number, increase foundStates
            s.erase(0,6);
            curState = atoi(s.c_str());
            foundStates++;
        }
		//look for hitbox if state was found
		if(s.substr(0,7) == "HITBOX=" && foundPath && foundHitboxes == foundStates - 1){
            s.erase(0,7);
			hsplitByComma(s, hitboxes[foundHitboxes++]);
        }
		//look for coordinates if path and a state are set
		if(s.substr(0,5) == "XYWH=" && foundPath && foundStates > 0){
            s.erase(0,5);
            sprite_clips[curState].push_back( splitByComma(s) );
        }

		if( foundPath && foundStates > 0 && foundXYWHs > 0 && foundHitboxes == foundStates) completed = true;
    }

	input_file.close();
	return completed;
}
