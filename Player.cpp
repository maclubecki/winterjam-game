#include "Player.h"
#include "Enemy.h"

Player::Player(float x, float y, Texture* _texture):
	Entity(x,y,_texture), shot_triggered(false), stamina(200), 
	exhausted(false), next_move_time(0)
{
	velocity = 50;
	width = _texture->getWidth();
	height = _texture->getHeight();
	hitbox = texture->getHitbox(sprite_state);
}
void Player::setShotCooldown(bool valid_target){
	const int shot_cooldown_hit = 500;	
	const int shot_cooldown_miss = 850;

	shot_triggered = false;
	words.clear();
}
void Player::reset(){
	setPosition(CHUDEJ_RESX/2, CHUDEJ_RESY/2);
	wp_active = false;
	moving = false;
	dying = false;
	destruction_trigger_on = false;
	shot_triggered = false;
	exhausted = false;
	stamina = 200;	
	words.clear();
	e_state = ENTITYSTATE_IDLE;
}
void Player::logicUpdate(float accumulator, SDL_Point *camera){
	if(exhausted){
		if(SDL_GetTicks() > next_move_time)
			exhausted = false;
		if(e_state != ENTITYSTATE_ATTACK)
			e_state = ENTITYSTATE_IDLE;
	}
	//movement based on stamina
	if(stamina > 0 && !exhausted){
		moving = false;
		move(accumulator, camera);		
		if(moving)
			stamina-=2;
	}
	//exhaustion check
	if(stamina <= 0 && !exhausted){
		const int move_cd = 1500;
		exhausted = true;
		next_move_time = SDL_GetTicks() + move_cd;
		e_state = ENTITYSTATE_IDLE;
		wp_active = false;
	}
	//regen
	if(stamina < 200)
		++stamina;

	setSide();
	//attack animation check
	if(e_state == ENTITYSTATE_ATTACK && animationDone()){
		if(moving)
			e_state = ENTITYSTATE_RUN;
		else
			e_state = ENTITYSTATE_IDLE;
	}
}