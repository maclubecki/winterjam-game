#include "Enemy.h"

Enemy::Enemy(float x, float y, const enemyType_t &e_t, const std::string &killword_, Texture* tex, Texture* word_texture):
	Entity(x,y,NULL), targeted(false), ability_activated(false), ability_activation_time(0), stun_time(0), type(e_t), got_hit(false),
	killword_texture(word_texture), killword(killword_)
{
	setTexture(tex);
	switch(e_t){
	case ENEMY_ZOMBIE:		
		velocity = 20;
		hp_cur = hp_max = 3;
		width = 98;
		height = 116;
		score = 50;
		break;
	case ENEMY_CRAB:
		velocity = 40;
		hp_cur = hp_max = 2;
		score = 75;
		break;
	case ENEMY_SHOOTER:
		velocity = SHOOTER_VELOCITY;
		hp_cur = hp_max = 2;
		score = 100;
		break;
	}

	setSpriteState(SPRITESTATE_RUN_D);
	setNextSprite();
}

void Enemy::logicUpdate(float accumulator, Player* hero, EntityContainer<Projectile> &p, SDL_Point *camera){
	if(stun_time > SDL_GetTicks())
		return;
	else if(e_state != ENTITYSTATE_ATTACK && e_state != ENTITYSTATE_DEATH)
		e_state = ENTITYSTATE_RUN;
	if(destruction_trigger_on) 
		return;
	moving = false;

	//check word for getting targeted
	if(hero->isTargeting(killword)){
		setTargeted(true);
		hero->setShotCooldown(true);
	}
	//check for collision with snowballs
	for(std::list<Projectile*>::iterator iter = p.begin(); iter != p.end(); ++iter){
		if((*iter)->intersects(*this) && !(*iter)->isDestructionTriggered() && !(*iter)->hasHitTarget() && (*iter)->isPlayerCreated() ){
			(*iter)->setDestruction(DESTRUCTION_DELAY_PROJECTILE);
			sufferDamage();
		}
	}
	//nothing special for zombie
	if(type == ENEMY_ZOMBIE){
		move(accumulator, camera);
		setSide();
		setWaypoint(hero->getXCenter(), hero->getYCenter());
	}
	//if a crab, check the jump
	else if(type == ENEMY_CRAB){
		move(accumulator, camera);
		setSide();
		setWaypoint(hero->getXCenter(), hero->getYCenter());

		const int jump_multiplier = 5, jump_l = 120, jump_dur = 150, jump_cd = 3000;

		//if already jumping, check if has to stop
		if(ability_activated){
			if(ability_activation_time + jump_dur < SDL_GetTicks()){
				ability_activated = false;
				velocity /= jump_multiplier;
				e_state = ENTITYSTATE_RUN;
			}
		}
		else{
			//if in range and time is right
			if(ability_activation_time + jump_cd < SDL_GetTicks() && distanceToTarget(hero) <= jump_l){
				ability_activation_time = SDL_GetTicks();
				velocity *= jump_multiplier;
				ability_activated = true;
				e_state = ENTITYSTATE_ATTACK;
			}
		}		
	}
	//if shooter, check for shot
	else if(type == ENEMY_SHOOTER){
		//shooter comes up to a distance <shot_range> and then stays there and shoots every <shot_cd>
		const int shot_range = 350, shot_cd = 5000;

		if(distanceToTarget(hero) > shot_range || getY() - killword_texture->getHeight() < 0 || getY() + hbH() > CHUDEJ_RESY ){
			velocity = SHOOTER_VELOCITY;
			move(accumulator, camera);			
			setWaypoint(hero->getXCenter(), hero->getYCenter());
		}
		else{
			setFacingDir(hero->getXCenter(), hero->getYCenter());
			velocity = 0;
			e_state = ENTITYSTATE_IDLE;
			if(ability_activation_time + shot_cd < SDL_GetTicks() && inBounds()){
				ability_activation_time = SDL_GetTicks();
				ability_activated = true;
				e_state = ENTITYSTATE_ATTACK;
			}
		}
	}

	//check for hero collision
	if(intersects(*hero)){
		hero->triggerDeath();
	}

}
void Enemy::resetWord(Texture* word_texture, const std::string &word){
	delete killword_texture;
	killword_texture = word_texture;
	killword = word;
}
void Enemy::sufferDamage(){
	--hp_cur;
	got_hit = true;

	if(hp_cur <= 0){
		dying = true;
		e_state = ENTITYSTATE_DEATH;
		setSpriteState(SPRITESTATE_DEATH);
		setDestruction(DESTRUCTION_DELAY_ENEMY);
	}
	else{
		stun_time = SDL_GetTicks() + STUN_DURATION;
		e_state = ENTITYSTATE_FLINCH;
	}
}