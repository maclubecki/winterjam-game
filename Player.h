#ifndef PLAYER_H
#define PLAYER_H

#include "Entity.h"

class Enemy;

class Player :
	public Entity
{
public:	

	Player(float x, float y, Texture* _texture);
	~Player(void){
	}

	std::string& getWords(){return words;}	
	bool isExhausted() const {return exhausted;}
	bool isTargeting(const std::string &killword){
		if(words.compare(killword) == 0 && shot_triggered)
			return true;
		return false;
	}
	void reset();
	void triggerShot(const std::string &s){
		shot_triggered = true;	
		words = s;
	}
	void triggerDeath(){
		dying = true;
		e_state = ENTITYSTATE_DEATH;
		setSpriteState(SPRITESTATE_DEATH);
	}
	int getStamina(){return stamina;}	
	void setShotCooldown(bool valid_target);
	void logicUpdate(float accumulator, SDL_Point *camera = NULL);

private:
	int stamina;
	bool shot_triggered;
	bool exhausted;
	Uint32 next_move_time;
	std::string words;	

	void updateBar();
};

#endif